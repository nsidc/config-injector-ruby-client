Gem::Specification.new do |s|
  s.name        = 'nsidc-config_injector_client'
  s.version     = '0.0.4'
  s.date        = '2014-08-28'
  s.summary     = "Ruby client for the NSIDC config injector service"
  s.description = "Simple interface for accessing and updating configuration data in NSIDC key value store service"
  s.authors     = ["Chris Chalstrom", "Michael Brandt"]
  s.email       = ''
  s.files       = ["lib/nsidc/config_injector_client.rb"]
  s.homepage    =
    'http://rubygems.org/gems/nsidc-config_injector_client'
  s.license       = 'MIT'

  s.add_dependency('rest-client')
  s.add_development_dependency('rspec')
end
