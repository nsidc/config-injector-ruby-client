require 'rest_client'
require 'json'
require 'uri'

module Nsidc
  class ConfigInjectorClient

    def initialize(service_endpoint, environment = 'production')
      environment = 'integration' if environment == 'development'

      if environment == 'production'
        @service_endpoint = service_endpoint
      else
        service_uri = URI.parse(service_endpoint)
        service_uri.host.prepend("#{environment}.")
        @service_endpoint = service_uri.to_s
      end
    end

    # Server returns json response of key value pair, value is null if not found.
    def get config_key
      response = RestClient.get config_route(config_key), { :accept => 'application/json' }
      JSON.parse(response)[config_key]
    end

    def put config_key, new_val
      RestClient.put config_route(config_key),
        { :val => new_val },
        { :accept => 'application/json' }
    end

    private
    def config_route config_key
      RestClient::Resource.new('').concat_urls @service_endpoint, config_key
    end

  end
end
