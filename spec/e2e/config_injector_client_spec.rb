require 'spec_helper'
require 'securerandom'

# These end to end tests expect a service to be up and running and :endpoint
# And they expect that :test_key exists in the db.
describe Nsidc::ConfigInjectorClient do
  let(:endpoint) { 'http://config-injector-vm.apps.int.nsidc.org:10680' }
  let(:test_key) { 'configval.1.2.3' }

  describe '#get' do
    it 'gets a value back from the test key' do
      environment = 'integration'
      client = Nsidc::ConfigInjectorClient.new endpoint, environment
      config_val = client.get test_key
      expect(config_val).to_not be_nil
    end
  end

  describe '#put' do
    it 'rewrites values when given a test key' do
      environment = 'integration'
      client = Nsidc::ConfigInjectorClient.new endpoint, environment
      new_config_val = SecureRandom.hex 16

      client.put test_key, new_config_val
      returned_config_val = client.get test_key

      expect(returned_config_val).to eq(new_config_val)
    end
  end

end
