require 'spec_helper'

describe Nsidc::ConfigInjectorClient do
  let(:endpoint) { 'http://config-injector-vm.apps.int.nsidc.org:10680' }

  describe '#initialize' do
    it 'sets @service_endpoint if no environment is given' do
      client = Nsidc::ConfigInjectorClient.new(endpoint)
      expect(client.instance_variable_get("@service_endpoint")).to eql endpoint
    end

    it 'sets @service_endpoint with the environment as a subdomain if it is given' do
      environment = 'qa'
      client = Nsidc::ConfigInjectorClient.new(endpoint, environment)
      expect(client.instance_variable_get("@service_endpoint")).to eql 'http://qa.config-injector-vm.apps.int.nsidc.org:10680'
    end

    it 'uses "integration" as the subdomain when "development" is passed as the environment' do
      environment = 'development'
      client = Nsidc::ConfigInjectorClient.new(endpoint, environment)
      expect(client.instance_variable_get("@service_endpoint")).to eql 'http://integration.config-injector-vm.apps.int.nsidc.org:10680'
    end

    it 'does not add a subdomain based on the environemnt when "production" is passed' do
      environment = 'production'
      client = Nsidc::ConfigInjectorClient.new(endpoint, environment)
      expect(client.instance_variable_get("@service_endpoint")).to eql endpoint
    end
  end

end
