# PROJECT STATUS

**The only known user of the config-injector service is the Swagger implementation. It should not be used for future projects. Consult the current development team tech radar (and the development team) for recommended approaches to credential handling.**

## Config Injector Ruby Client

This is a ruby client that is intended to be used with a configuration service
that acts as a key/value store. The code base for the configuration service is
available at: https://bitbucket.org/nsidc/config-injector-service/overview

## Usage

In order to use the ruby client, you must have network access to an instance of
the configuration service which is defined at
https://bitbucket.org/nsidc/config-injector-service/overview.

### GET
```
client = Nsidc::ConfigInjectorClient.new config_service_path
configuration_value = client.get(configuration_key)
```

### PUT
```
client = Nsidc::ConfigInjectorClient.new config_service_path
client.put(configuration_key, new_configuration_value)
```

## Development

Just run `bundle install` and you should be good to go for development. It would
also be useful to get an instance of the service up and running. More detailed
information about the service can be found at
https://bitbucket.org/nsidc/config-injector-service/overview

## Testing
`bundle install; bundle exec rspec;`

Note: Integration tests in spec/e2e are expecting an instance of the
configuration service to be up and running. This is so the functionality of the
ruby client can be verified against a live instance of the service.  If you
don't have the service running, these tests will fail.

## Organization Info

### How to contact NSIDC

User Services and general information:  
Support: http://support.nsidc.org  
Email: nsidc@nsidc.org  

Phone: +1 303.492.6199  
Fax: +1 303.492.2468  

Mailing address:  
National Snow and Ice Data Center  
CIRES, 449 UCB  
University of Colorado  
Boulder, CO 80309-0449 USA  

### License

Every file in this repository is covered by the GNU GPL Version 3; a copy of the
license is included in the file COPYING.

### Citation Information

Chris Chalstrom and Michael Brandt (2014): Config Injector Ruby Client. The
National Snow and Ice Data Center. Software.
http://ezid.cdlib.org/id/doi:10.7265/N59884ZG